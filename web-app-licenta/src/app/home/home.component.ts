import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SignalRService } from '../shared/signal-r.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  userDetails;
  isAdmin: boolean = false;
  constructor(private router: Router,
      public service: UserService,
      public signalRService: SignalRService,
      private http: HttpClient,
      private toastr: ToastrService) { }

  ngOnInit(): void {
    // this.signalRService.startConnection();
    // this.signalRService.addTransferSensorsDataListener();

    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payLoad.role;
    if(userRole === 'Admin'){
      this.isAdmin = true;
    }

    this.service.getUserProfile()
      .subscribe(
        (res:any) => {
          this.userDetails = res;
        },
        err => {
          console.log(err);
        }
      )
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(["/user/login"]);
  }

  onSubmit(){
    this.service.update()
      .subscribe((response: any) => {
        this.toastr.success(response.message, 'Success');
        this.service.getUserProfile()
            .subscribe(
                (res:any) => {
                  this.userDetails = res;
                },
                err => {
                  console.log(err);
              });
      },
      (err) => {
        this.toastr.error('Invalid password', 'Fail')
      });
    }
}
