import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@aspnet/signalr';
import { DualLedLightSensitiveModel } from 'src/app/interfaces/dual-led-light-sensitive-model';
import { SmdLedModel } from 'src/app/interfaces/smd-led-model';
import { ProcessSensorsDataService } from 'src/app/shared/process-sensors-data.service';
import { SignalRService } from 'src/app/shared/signal-r.service';
import { UserService } from 'src/app/shared/user.service';
import * as moment from 'moment';
@Component({
  selector: 'app-appliances',
  templateUrl: './appliances.component.html',
  styleUrls: ['./appliances.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppliancesComponent implements OnInit {

  userDetails
  isFireAlarmOn: boolean;
  isMovementDetected: boolean;
  temperatureValue;
  valueLedLightBlocking;
  dualLedData: DualLedLightSensitiveModel;
  smdLedData: SmdLedModel;
  now: string = "";
  ledStateTouch: boolean;
  constructor(
    public signalRService: SignalRService,
    public processSensorsDataService: ProcessSensorsDataService) { }

  ngOnInit(): void {
    this.signalRService.startConnection();
    this.signalRService.addTransferSensorsDataListener().on('transfersensorsdata', (data: string) => {
      let stringData = JSON.parse(data);
      console.log(stringData.smd_control);
      this.now = moment().hour() + ":" + moment().minute() + ":" + moment().second();
      //1. Fire Alarm info from sensors
      this.isFireAlarmOn = this.processSensorsDataService.processFireAlarmData(stringData["senzor_incendiu"]);

      //2. Movement detection with LED on on detection
      this.isMovementDetected = this.processSensorsDataService.processPIRSensorData(stringData["value_pir_led"]);

      //3. Temperature
      this.temperatureValue = this.processSensorsDataService.processTemperatureValue(stringData);

      //4. Draperie
      this.valueLedLightBlocking = this.processSensorsDataService.processLedLightBlocking(stringData["value_led_light_blocking"]);

      //5. Dual Led Light Sensitive
      this.dualLedData = this.processSensorsDataService.processDualLedLightSensitive(stringData["dual_led_light_sensitive"]);

      //6. SMD Led
      this.smdLedData = this.processSensorsDataService.processSmdLed(stringData["smd_control"]);

      //7. Touch Control Led
      this.ledStateTouch = this.processSensorsDataService.processLedStateTouch(stringData["led_state_touch"]);
    });
  }





}
