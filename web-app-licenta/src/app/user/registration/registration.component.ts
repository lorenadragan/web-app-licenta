import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(public userService: UserService,
    public toastr: ToastrService) { }

  ngOnInit(): void {
    this.userService.registrationModel.reset();
  }

  onSubmit() {
    this.userService.register().subscribe(
      (res:any) => {
        if(res.succeeded){
          this.userService.registrationModel.reset();
          this.toastr.success('New User', 'Registration Successfull');
        }
        else {
          res.errors.forEach(element => {
            switch(element.code){
              case 'DuplicateUserName' :
                this.toastr.error('Registration Failed', 'This Username already exists');
              break;
              default:

                this.toastr.error('Registration Failed', 'Error in registering new account');
              break;
            }
          });
        }
      },
      (err) => {
        if(err.error === "There is no house with given access code")
          this.toastr.error("There is no house with given access code", "Registration Failed");
        else
          console.log(err.error);
      }
    );

  }

}
