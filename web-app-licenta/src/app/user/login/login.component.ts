import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public userService: UserService,
      private router: Router,
      private toastr: ToastrService) { }

  ngOnInit(): void {
    this.userService.authenticationModel.reset();

    if(localStorage.getItem('token') !== null){
      this.router.navigateByUrl('/app-menu');
    }
  }

  onSubmit() {
    this.userService.login()
      .subscribe(
        (response: any) => {
          //the token is return => save it in localStorage
          localStorage.setItem('token', response.token);
          this.router.navigateByUrl('/app-menu');
        },
        (err) => {
          if(err.status === 400 ){
            this.toastr.error('Incorrect username or password', 'Authentication Failed');
          }
          else {
            console.log(err);
          }
        }
      );
  }

}
