import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurtainsLedComponent } from './curtains-led.component';

describe('CurtainsLedComponent', () => {
  let component: CurtainsLedComponent;
  let fixture: ComponentFixture<CurtainsLedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurtainsLedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurtainsLedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
