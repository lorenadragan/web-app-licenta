import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'curtains-led',
  templateUrl: './curtains-led.component.html',
  styleUrls: ['./curtains-led.component.css']
})
export class CurtainsLedComponent implements OnInit {

  @Input() isCurtainPulled: boolean;

  enable: boolean;
  power: boolean;
  constructor() { }

  ngOnInit(): void {
  }

  onFlagChange(event){
  }

}
