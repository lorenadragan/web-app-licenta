import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'touch-led',
  templateUrl: './touch-led.component.html',
  styleUrls: ['./touch-led.component.css']
})
export class TouchLedComponent implements OnInit {
  @Input() isLedOn: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
