import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TouchLedComponent } from './touch-led.component';

describe('TouchLedComponent', () => {
  let component: TouchLedComponent;
  let fixture: ComponentFixture<TouchLedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TouchLedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TouchLedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
