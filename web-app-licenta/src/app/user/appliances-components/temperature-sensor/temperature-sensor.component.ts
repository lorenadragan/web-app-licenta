import "core-js/stable";
import "regenerator-runtime/runtime";
import { Component, ViewChild, OnInit,  ViewEncapsulation, Input, OnChanges, SimpleChanges, AfterViewInit, Renderer2 } from '@angular/core';
import { ITooltipRenderEventArgs, LinearGaugeComponent } from '@syncfusion/ej2-angular-lineargauge';
import { ContainerType, Orientation, ILoadedEventArgs, LinearGaugeTheme } from '@syncfusion/ej2-lineargauge';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_dark from "@amcharts/amcharts4/themes/dark";
// Importing translations
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import { DecimalPipe } from "@angular/common";
@Component({
  selector: 'temperature-sensor',
  templateUrl: './temperature-sensor.component.html',
  styleUrls: ['./temperature-sensor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TemperatureSensorComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() temperatureValue;
  hand: am4charts.ClockHand;


  constructor(private renderer: Renderer2,
    private _decimalPipe: DecimalPipe) { }
  ngOnChanges(changes: SimpleChanges){
      if(changes.temperatureValue){
        if(this.hand){
          let nr: number = changes.temperatureValue.currentValue
          let nrRounded: number = +this._decimalPipe.transform(nr, "2.1-1");
          this.hand.showValue(nrRounded);
        }
      }
  }

  ngAfterViewInit(): void {
      am4core.useTheme(am4themes_kelly);
      am4core.useTheme(am4themes_animated);
      let chart = am4core.create("chartdiv", am4charts.GaugeChart);

      chart.hiddenState.properties.opacity = 0;

      chart.innerRadius = -25;

      let axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());

      axis.min = 0;
      axis.max = 100;
      axis.strictMinMax = true;
      axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor("background");
      axis.renderer.grid.template.strokeOpacity = 0.3;

      let colorSet = new am4core.ColorSet();

      let range0 = axis.axisRanges.create();
      range0.value = 0;
      range0.endValue = 50;
      range0.axisFill.fillOpacity = 1;
      range0.axisFill.fill = colorSet.getIndex(0);
      range0.axisFill.zIndex = - 1;

      let range1 = axis.axisRanges.create();
      range1.value = 50;
      range1.endValue = 80;
      range1.axisFill.fillOpacity = 1;
      range1.axisFill.fill = colorSet.getIndex(2);
      range1.axisFill.zIndex = -1;

      let range2 = axis.axisRanges.create();
      range2.value = 80;
      range2.endValue = 100;
      range2.axisFill.fillOpacity = 1;
      range2.axisFill.fill = colorSet.getIndex(4);
      range2.axisFill.zIndex = -1;

      this.hand = chart.hands.push(new am4charts.ClockHand());
  }

  ngOnInit(): void {
      // this.Axes[0].pointers[0].value = 26;
  }
}
