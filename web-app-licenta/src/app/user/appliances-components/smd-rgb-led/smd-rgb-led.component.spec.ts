import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmdRgbLedComponent } from './smd-rgb-led.component';

describe('SmdRgbLedComponent', () => {
  let component: SmdRgbLedComponent;
  let fixture: ComponentFixture<SmdRgbLedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmdRgbLedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmdRgbLedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
