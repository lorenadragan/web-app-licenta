import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'smd-rgb-led',
  templateUrl: './smd-rgb-led.component.html',
  styleUrls: ['./smd-rgb-led.component.css']
})
export class SmdRgbLedComponent implements OnInit, OnChanges {

  @Input() blue_intensity: number;
  @Input() green_intensity: number;
  @Input() red_intensity: number;
  @Input() switch_state_smd: number;
  @Input() is_auto_smd_light: boolean;

  enable: boolean;
  power: boolean;
  ledIntensityRed: number = 100;
  ledIntensityGreen: number = 100;
  ledIntensityBlue: number = 100;

  optionsRed: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor: (value: number) => {
      return 'red'
    },
    showSelectionBar: true,
    getSelectionBarColor: (value: number) => {
      return 'red'
    },
  }
  optionsGreen: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor: (value: number) => {
      return 'green'
    },
    showSelectionBar: true,
    getSelectionBarColor: (value: number) => {
      return 'green'
    },
  }
  optionsBlue: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor: (value: number) => {
      return 'blue'
    },
    showSelectionBar: true,
    getSelectionBarColor: (value: number) => {
      return 'blue'
    },
  }

  constructor(private http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges){
    if(changes.switch_state_smd && changes.switch_state_smd.currentValue !== null){
      changes.switch_state_smd.currentValue == 1 ? this.power = true : this.power = false;
    }

    if(changes.is_auto_smd_light && changes.is_auto_smd_light.currentValue !== null){
      this.enable = changes.is_auto_smd_light.currentValue;
    }

    if(changes.red_intensity && changes.red_intensity.currentValue !== null){
      let pwmRed = changes.red_intensity.currentValue;
      this.ledIntensityRed = (pwmRed * 255) / 100;
    }
    if(changes.green_intensity && changes.green_intensity.currentValue !== null){
      let pwmGreen = changes.green_intensity.currentValue;
      this.ledIntensityGreen = (pwmGreen * 255) / 100;
    }
    if(changes.blue_intensity && changes.blue_intensity.currentValue !== null){
      let pwmBlue = changes.blue_intensity.currentValue;
      this.ledIntensityBlue = (pwmBlue * 255) / 100;
    }
  }

  ngOnInit(): void {

  }

  seteazaCulori(){
    let redForPwm = (this.ledIntensityRed * 255) / 100;
    let greenForPwm = (this.ledIntensityGreen * 255) / 100;
    let blueForPwm = (this.ledIntensityBlue * 255) / 100;
    console.log(redForPwm);
    console.log(greenForPwm);
    console.log(blueForPwm);
    let string = "http://localhost:59899/api/sensors/rgb/manual/red=" + redForPwm + "/green=" + greenForPwm + "/blue=" + blueForPwm;
    this.http.get(string).subscribe();
  }

  onEnableChange(event){
    if(event == true){
      this.http.get("http://localhost:59899/api/sensors/smd-rgb/auto").subscribe();
    }
    if(event == false){
      let redForPwm = (this.ledIntensityRed * 255) / 100;
      let greenForPwm = (this.ledIntensityGreen * 255) / 100;
      let blueForPwm = (this.ledIntensityBlue * 255) / 100;
      let string = "http://localhost:59899/api/sensors/rgb/manual/red=" + redForPwm + "/green=" + greenForPwm + "/blue=" + blueForPwm;
      this.http.get(string).subscribe();
    }
  }

  redHasChanged(event){

  }
  greenHasChanged(event){

  }
  blueHasChanged(event){

  }
}
