import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'fire-alarm',
  templateUrl: './fire-alarm.component.html',
  styleUrls: ['./fire-alarm.component.css']
})
export class FireAlarmComponent implements OnInit, OnChanges {
  @Input() fireAlarm: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

  }

}
