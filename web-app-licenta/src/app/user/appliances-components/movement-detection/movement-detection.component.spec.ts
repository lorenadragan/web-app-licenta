import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementDetectionComponent } from './movement-detection.component';

describe('MovementDetectionComponent', () => {
  let component: MovementDetectionComponent;
  let fixture: ComponentFixture<MovementDetectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementDetectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementDetectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
