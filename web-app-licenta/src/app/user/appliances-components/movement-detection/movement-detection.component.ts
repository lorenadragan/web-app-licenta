import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'movement-detection',
  templateUrl: './movement-detection.component.html',
  styleUrls: ['./movement-detection.component.css']
})
export class MovementDetectionComponent implements OnInit {

  @Input() isMovementDetected: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
