import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
import { DualLedLightSensitiveModel } from 'src/app/interfaces/dual-led-light-sensitive-model';
import { HttpClient } from '@angular/common/http';
/*
dual_led_light_sensitive: {
	is_off_dual_led_photoresistor: false,
	is_auto_dual_led_photoresistor: true,
	value_red_intensity_dual: 120,
	value_green_intensity_dual: 120,
	value_intensity_auto: 150
}
*/

@Component({
  selector: 'dual-led-light-sensitive',
  templateUrl: './dual-led-light-sensitive.component.html',
  styleUrls: ['./dual-led-light-sensitive.component.css']
})

export class DualLedLightSensitiveComponent implements OnInit, OnChanges {
  @Input() is_off_dual_led_photoresistor: boolean;
  @Input() is_auto_dual_led_photoresistor: boolean;
  @Input() value_red_intensity_dual;
  @Input() value_green_intensity_dual;
  @Input() value_intensity_auto;

  @ViewChild("dual_led_img") imageHTML;

  valueRed: number = 100;
  valueGreen: number = 100;

  enable: boolean;
  power: boolean;
  powerInput: boolean;

  optionsRed: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor: (value: number) => {
      return 'red'
    },
    showSelectionBar: true,
    getSelectionBarColor: (value: number) => {
      return 'red'
    },
  }
  optionsGreen: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor: (value: number) => {
      return 'green'
    },
    showSelectionBar: true,
    getSelectionBarColor: (value: number) => {
      return 'green'
    },
  }

  constructor(public http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges){
    if(changes.is_off_dual_led_photoresistor && changes.is_off_dual_led_photoresistor.currentValue !== null){
      this.power = !changes.is_off_dual_led_photoresistor.currentValue; //power e true initial pentru ca is_off la prima rulare vine mereu fals
    }
    if(changes.is_auto_dual_led_photoresistor && changes.is_auto_dual_led_photoresistor !== null){
      this.enable = changes.is_auto_dual_led_photoresistor.currentValue;
    }
    if(changes.value_red_intensity_dual && changes.value_red_intensity_dual !== null){
      this.valueRed = (changes.value_red_intensity_dual.currentValue * 100) / 255;
    }
    if(changes.value_green_intensity_dual && changes.value_green_intensity_dual !== null){
      this.valueGreen = (changes.value_green_intensity_dual.currentValue * 100) / 255;
    }
  }

  ngOnInit(){
  }

  onEnableChange(event){
    if(event == false) {
      //trec pe manual
      var value_red_for_pwm = (this.valueRed * 255) / 100;
      var value_green_for_pwm = (this.valueGreen * 255) / 100;
      this.http.get("http://localhost:59899/api/sensors/photoresistor-led/manual/red=" + value_red_for_pwm + "/green=" + value_green_for_pwm).subscribe();
    }
    if(event == true){
      this.http.get("http://localhost:59899/api/sensors/photoresistor-led/auto").subscribe();
    }
  }

  onPowerChange(event){
    if(event === false){
      this.http.get("http://localhost:59899/api/sensors/photoresistor-led/off").subscribe();
    }
    if(event == true){
      this.http.get("http://localhost:59899/api/sensors/photoresistor-led/on").subscribe();
    }
  }

  handleRedSlider(event){
    var value_red_for_pwm = (event.value * 255) / 100;
    var value_green_for_pwm = (this.valueGreen * 255) / 100;
    this.http.get("http://localhost:59899/api/sensors/photoresistor-led/manual/red=" + value_red_for_pwm + "/green=" + value_green_for_pwm).subscribe();
  }

  handleGreenSlider(event){
    var value_red_for_pwm = (this.valueRed * 255) / 100;
    var value_green_for_pwm = (event.value * 255) / 100;
    this.http.get("http://localhost:59899/api/sensors/photoresistor-led/manual/red=" + value_red_for_pwm + "/green=" + value_green_for_pwm).subscribe();
  }

  private _setImageOpacity(){
    // if(this.imageHTML){
              //   console.log(this.imageHTML.nativeElement)
              //   if(this.valueGreen !== 0){
              //     this.imageHTML.nativeElement.setAttribute('style', 'opacity:' + this.valueGreen/100);
              //   }
              //   else {
              //     this.imageHTML.nativeElement.setAttribute('style', 'opacity:0');
              //   }
              // }
  }

}
