import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DualLedLightSensitiveComponent } from './dual-led-light-sensitive.component';

describe('DualLedLightSensitiveComponent', () => {
  let component: DualLedLightSensitiveComponent;
  let fixture: ComponentFixture<DualLedLightSensitiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DualLedLightSensitiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DualLedLightSensitiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
