import { Injectable } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  registrationModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword: ['', Validators.required]
    }, {validator : this.comparePasswords}),
    AccessCode: ['', [Validators.required, Validators.minLength(10)]],
    // Role: ['', Validators.required]
    // Role: 'Customer'
  });

  authenticationModel = this.fb.group({
    UserName: ['', Validators.required],
    Password: ['', [Validators.required, Validators.minLength(6)]],
  });

  updateModel = this.fb.group({
    UserName: ['', Validators.required],
    FullName: ['', Validators.required],
    Password: ['', [Validators.required, Validators.minLength(6)]]
  })

//59899 acum, inainte 59899
  readonly BaseURI: string = "http://localhost:59899/api";
  constructor(private fb: FormBuilder, private http: HttpClient) { }

  register(): Observable<any> {
    var body = {
      UserName: this.registrationModel.value.UserName,
      FullName: this.registrationModel.value.FullName,
      Email: this.registrationModel.value.Email,
      AccessCode: this.registrationModel.value.AccessCode,
      Password: this.registrationModel.value.Passwords.Password,
      // Role: this.registrationModel.value.Role
      Role: 'Customer'
    };
    return this.http.post(this.BaseURI + '/ApplicationUser/Register', body);
  }


  login(): Observable<any> {
    var body = {
      UserName: this.authenticationModel.value.UserName,
      Password: this.authenticationModel.value.Password,
    };

    return this.http.post(this.BaseURI + '/ApplicationUser/Login', body);
  }

  getUserProfile(): Observable<any> {
    return this.http.get(this.BaseURI + '/UserProfile');
  }

  roleMatch(allowedRoles): boolean{
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if(userRole == element){
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }

  update(): Observable<any> {
    var body = {
      FullName : this.updateModel.value.FullName,
      Password: this.updateModel.value.Password,
      UserName: this.updateModel.value.UserName
    };
    return this.http.put(this.BaseURI + "/UserProfile/Update/User", body);
  }

  /**
   * Custom Validators for password
   * param1: type AbstractControl, the form control passed as an input
   * return: object of a key-value pair if the validation fails
   *      if validation fails, it returns an object, which has key and a value.
   *      Key contains the name of the error and value is always true
   *      if validation passes, it returns null
   */
  uppercaseValidator(control: AbstractControl): {[key: string]: boolean} | null {
    if(!control.value.match(new RegExp("[A-Z]+"))){
      return {'uppercase': true}
    }
    return null;
  }
  lowercaseValidator(control: AbstractControl): {[key: string]: boolean} | null {
    if(!control.value.match(new RegExp("[a-z]+"))){
      return {'lowercase': true}
    }
    return null;
  }
  nonAlphanumericalValidator(control: AbstractControl): {[key: string]: boolean} | null {
    if(!control.value.match(new RegExp("[^a-zA-Z\\d\\s:]"))){
      return {'nonalpha': true}
    }
    return null;
  }
  comparePasswords(fb: FormGroup){
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    //passwordMismatch
    //confirmPswrdCtrl.errors={passwordMismatch:true}
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }
}
