import { Injectable } from '@angular/core';
import { Sensors } from '../interfaces/sensors.model';
import * as signalR from "@aspnet/signalr";


@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public data: Sensors;
  private hubConnection: signalR.HubConnection;

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
                              .withUrl('http://localhost:59899/sensors')
                              .withAutomaticReconnect()
                              .configureLogging(signalR.LogLevel.Information)
                              .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  public addTransferSensorsDataListener = () => {
    return this.hubConnection;
  }
}
