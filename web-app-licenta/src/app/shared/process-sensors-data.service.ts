import { Injectable } from '@angular/core';
import { DualLedLightSensitiveModel } from '../interfaces/dual-led-light-sensitive-model';
import { SmdLedModel } from '../interfaces/smd-led-model';

@Injectable({
  providedIn: 'root'
})
export class ProcessSensorsDataService {

  constructor() { }

  processFireAlarmData(fireData): boolean {
    if(fireData == 1){
      return true;
    }
    else {
      return false;
    }
  }

  processPIRSensorData(movementData): boolean {
    if(movementData == 1){
      return true;
    }
    else {
      return false;
    }
  }

  processTemperatureValue(temperatureData) {
    return temperatureData["value_temperature"];
  }

  processLedLightBlocking(curtainData){
    if(curtainData == 1){
      return true;
    }
    else {
      return false;
    }
  }

  processDualLedLightSensitive(dualLedData: DualLedLightSensitiveModel){
    return dualLedData;
  }

  processSmdLed(smdLedData: SmdLedModel){
    return smdLedData;
  }

  processLedStateTouch(ledData){
    if(ledData == 1){
      return true; //e aprins
    }
    else if(ledData == 0){
      return false;
    }
  }
}
