import { TestBed } from '@angular/core/testing';

import { ProcessSensorsDataService } from './process-sensors-data.service';

describe('ProcessSensorsDataService', () => {
  let service: ProcessSensorsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcessSensorsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
