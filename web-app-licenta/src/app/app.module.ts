import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { LinearGaugeModule } from '@syncfusion/ej2-angular-lineargauge';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { UiSwitchModule } from 'ngx-ui-switch';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/user.service';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { CreateClientModalComponent } from './admin-handlers/create-client-modal/create-client-modal.component';
import { ApplicationMenuComponent } from './user/application-menu/application-menu.component';
import { AdminHandlerService } from './admin-handlers/admin-handler.service';
import { SignalRService } from './shared/signal-r.service';
import { AppliancesComponent } from './user/appliances/appliances.component';
import { FireAlarmComponent } from './user/appliances-components/fire-alarm/fire-alarm.component';
import { MovementDetectionComponent } from './user/appliances-components/movement-detection/movement-detection.component';
import { TemperatureSensorComponent } from './user/appliances-components/temperature-sensor/temperature-sensor.component';
import { DualLedLightSensitiveComponent } from './user/appliances-components/dual-led-light-sensitive/dual-led-light-sensitive.component';
import { SmdRgbLedComponent } from './user/appliances-components/smd-rgb-led/smd-rgb-led.component';
import { CurtainsLedComponent } from './user/appliances-components/curtains-led/curtains-led.component';
import { TouchLedComponent } from './user/appliances-components/touch-led/touch-led.component';
import { DecimalPipe } from '@angular/common';
import { CreateAccessCodeModalComponent } from './admin-handlers/create-access-code-modal/create-access-code-modal.component';
import { UpdateClientModalComponent } from './admin-handlers/update-client-modal/update-client-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    CreateClientModalComponent,
    ApplicationMenuComponent,
    AppliancesComponent,
    FireAlarmComponent,
    MovementDetectionComponent,
    TemperatureSensorComponent,
    DualLedLightSensitiveComponent,
    SmdRgbLedComponent,
    CurtainsLedComponent,
    TouchLedComponent,
    CreateAccessCodeModalComponent,
    UpdateClientModalComponent
  ],
  imports: [
    BrowserModule,
    LinearGaugeModule,
    NgxSliderModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    UiSwitchModule,
    ToastrModule.forRoot({
      progressBar: true
    }),
    TooltipModule.forRoot(),
    FormsModule,
  ],
  providers: [UserService, AdminHandlerService,SignalRService, DecimalPipe,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
