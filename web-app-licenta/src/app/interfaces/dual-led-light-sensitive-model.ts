export interface DualLedLightSensitiveModel{
  is_off_dual_led_photoresistor: boolean;
  is_auto_dual_led_photoresistor: boolean;
  value_red_intensity_dual: number;
  value_green_intensity_dual: number;
  value_intensity_auto: number;
}
