export interface SmdLedModel {
  blue_intensity: number;
  red_intensity: number;
  green_intensity: number;
  is_auto_smd_light: boolean;
  switch_state_smd: number;
}
