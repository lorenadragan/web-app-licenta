import { TestBed } from '@angular/core/testing';

import { AdminHandlerService } from './admin-handler.service';

describe('AdminHandlerService', () => {
  let service: AdminHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
