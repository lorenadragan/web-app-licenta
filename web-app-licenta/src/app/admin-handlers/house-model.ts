export interface HouseModel {
  accessCode: string;
  address: string;
  surface: number;
  roomsNumber: number;
}
