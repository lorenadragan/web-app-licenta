import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminHandlerService } from '../admin-handler.service';

@Component({
  selector: 'app-create-access-code-modal',
  templateUrl: './create-access-code-modal.component.html',
  styleUrls: ['./create-access-code-modal.component.css']
})
export class CreateAccessCodeModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    public adminService: AdminHandlerService
    ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.adminService.addNewHouse();
  }

}
