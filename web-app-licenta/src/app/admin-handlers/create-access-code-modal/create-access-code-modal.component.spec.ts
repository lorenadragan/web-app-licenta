import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccessCodeModalComponent } from './create-access-code-modal.component';

describe('CreateAccessCodeModalComponent', () => {
  let component: CreateAccessCodeModalComponent;
  let fixture: ComponentFixture<CreateAccessCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAccessCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccessCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
