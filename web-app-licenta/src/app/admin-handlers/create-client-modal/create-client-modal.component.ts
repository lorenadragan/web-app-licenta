import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminHandlerService } from '../admin-handler.service';
@Component({
  selector: 'app-create-client-modal',
  templateUrl: 'create-client-modal.component.html',
  styleUrls: ['create-client-modal.component.css']
})
export class CreateClientModalComponent implements OnInit {

  @Input() name;

  constructor(public activeModal: NgbActiveModal,
      private fb: FormBuilder,
      public adminService: AdminHandlerService) {}

  ngOnInit(){
  }

  onSubmit(){
    this.adminService.addNewClient();
  }
}
