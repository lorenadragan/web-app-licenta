export class ClientProfile {
  public fullName: string;
  public email: string;
  public accessCode: string;
  public userName: string;
  public id: string;

  constructor(fullName?: string, email?: string, accessCode?: string, userName?: string, id?: string){
    this.fullName = fullName;
    this.email = email;
    this.accessCode = accessCode;
    this.userName = userName;
    this.id = id;
  }
}
