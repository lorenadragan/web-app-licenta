import { string } from '@amcharts/amcharts4/core';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminHandlerService } from '../admin-handler.service';

@Component({
  selector: 'app-update-client-modal',
  templateUrl: './update-client-modal.component.html',
  styleUrls: ['./update-client-modal.component.css']
})
export class UpdateClientModalComponent implements OnInit {

  @Input() FullName: string;
  @Input() UserName: string;
  @Input() Email: string;
  @Input() AccessCode: string;
  @Input() Id: string;

  constructor(public activeModal: NgbActiveModal,
    public adminService: AdminHandlerService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.adminService.updateClient(this.Id);
  }

}
