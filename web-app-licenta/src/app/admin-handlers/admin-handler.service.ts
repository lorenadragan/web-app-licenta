import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { UserService } from '../shared/user.service';
import { ClientProfile } from './client-profile';

@Injectable({
  providedIn: 'root'
})
export class AdminHandlerService {
  readonly BaseURI: string = "http://localhost:59899/api";
  addClientModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Password: ['', [Validators.required, Validators.minLength(6)]],
    AccessCode: ['', [Validators.required, Validators.minLength(10)]],
    Role: ['Customer']
  })

  addHouseModel = this.fb.group({
    Address: ['', Validators.required],
    Surface: ['', Validators.required],
    RoomsNumber: ['', Validators.required],
    AccessCode: ['', [Validators.required, Validators.minLength(10)]]
  });

  updateClientModel = this.fb.group({
    FullName: ['', Validators.required],
    Email: ['', [Validators.email, Validators.required]],
    UserName: ['', Validators.required],
    AccessCode: ['', [Validators.required, Validators.minLength(10)]]
  });

  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private userService: UserService,
    private toastr: ToastrService) { }


  updateClient(Id: string): void {
    var body = {
      UserName: this.updateClientModel.value.UserName,
      FullName: this.updateClientModel.value.FullName,
      AccessCode: this.updateClientModel.value.AccessCode,
      Email: this.updateClientModel.value.Email,
      Id: Id
    }
    this.http.put(this.BaseURI + "/UserProfile/Admin/UpdateUser", body).subscribe((res: any) => {
      this.toastr.success(res.message, 'Success');
    }, (err: any) => {
      this.toastr.error(err.message, 'Fail')
    })
    console.log(body);
    this.updateClientModel.reset();
  };
  addNewClient(): void{
    var body = {
      UserName: this.addClientModel.value.UserName,
      FullName: this.addClientModel.value.FullName,
      Email: this.addClientModel.value.Email,
      AccessCode: this.addClientModel.value.AccessCode,
      Password: this.addClientModel.value.Password,
      Role: this.addClientModel.value.Role
    };

    console.log(body);
    this.http.post(this.BaseURI + "/UserProfile/NewClient", body).subscribe((res: any) => {
      this.toastr.success(res.message, 'Success');
    }, (err: any) => {
      this.toastr.error(err.message, 'Fail');
    });
    this.addClientModel.reset();
  }

  addNewHouse(){
    var body = {
      Address: this.addHouseModel.value.Address,
      Surface: this.addHouseModel.value.Surface,
      RoomsNumber: this.addHouseModel.value.RoomsNumber,
      AccessCode : this.addHouseModel.value.AccessCode
    };

    console.log(body);
    this.http.post(this.BaseURI + "/UserProfile/Add/AccessCode", body).subscribe((res: any) => {
      this.toastr.success(res.message, 'Success');
    }, (err: any) => {
      this.toastr.error(err.message, "Fail")
    });
    this.addHouseModel.reset()
  }

  getAllClients(): Observable<any> {
    return this.http.get(this.BaseURI + '/UserProfile/GetAllClients');
  }

  getAllHouses(): Observable<any> {
    return this.http.get(this.BaseURI + "/UserProfile/GetHouses/AdminPanel");
  }

  deleteClient(id: string) {
    return this.http.delete(this.BaseURI + '/UserProfile/DeleteClient/' + id, { responseType: 'text' as 'json' });
  }
}
