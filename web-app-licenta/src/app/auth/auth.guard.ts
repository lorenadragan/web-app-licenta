import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {

  constructor(private router: Router,
      private userService: UserService){}

  /*
  Check if the user is authenticated or not
  If it is authenticated, the the user will not be able to acces private routes, like the login page, from url
  If the user is not authenticated, it will not navigate to home page, but will be redirected to login page
  */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(localStorage.getItem('token') !== null){

        //here we check the role of the client required to acces a specified page
        //adica ce am setat in routing-module

        let roles = next.data['permittedRoles'] as Array<string>;
        //daca exista roluri necesare, verific daca clientul curent are aceste roluri pentru a putea sa mearga mai departe.
        if(roles) {
          if(this.userService.roleMatch(roles)){
            //the user has the required roles to access the current route
            return true;
          }
          else {
            this.router.navigate(['/forbidden']);
            return false;
          }
        }
        return true;
      }
      else {
        this.router.navigate(['/user/login']);
        return false; //now the user will not be able to access this route
      }

  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}
