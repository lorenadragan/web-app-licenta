import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { AdminHandlerService } from '../admin-handlers/admin-handler.service';
import { ClientProfile } from '../admin-handlers/client-profile';
import { CreateAccessCodeModalComponent } from '../admin-handlers/create-access-code-modal/create-access-code-modal.component';
import { CreateClientModalComponent } from '../admin-handlers/create-client-modal/create-client-modal.component';
import { HouseModel } from '../admin-handlers/house-model';
import { UpdateClientModalComponent } from '../admin-handlers/update-client-modal/update-client-modal.component';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  clientTableHeaders = ["FullName", "Email", "AccessCode", "UserName", "Actions"];
  housesHeaders = ["Address", "AccessCode", "Surface", "RoomsNumber"]
  clientProfiles: ClientProfile[] = [];
  houses: HouseModel[] = [];

  constructor(private modalService: NgbModal,
        private _adminService: AdminHandlerService) { }

  ngOnInit(): void {
    this._getClientProfiles();
  }

  private  _getClientProfiles(){
    this.houses = [];
    this.clientProfiles = [];
    this._adminService.getAllClients().subscribe((data) => {
      data.forEach(element => {
        var profile = new ClientProfile(element.fullName, element.email, element.accessCode, element.userName, element.id);
        this.clientProfiles.push(profile);
      });
    });

    this._adminService.getAllHouses().subscribe((data: HouseModel[]) => {
      data.forEach(element => {
        if(element.accessCode != "test123456"){
          this.houses.push(element);
        }
      });

      console.log(data);
    });

  }

  addClient() {
    const modalRef = this.modalService.open(CreateClientModalComponent).result.then(() => {
      this._getClientProfiles();
    });
  }

  deleteClient(profile?: ClientProfile){
    this._adminService.deleteClient(profile.id)
      .subscribe(() => {
        this._getClientProfiles();
      })
  }

  addAccessCode(){
    const modalRef = this.modalService.open(CreateAccessCodeModalComponent).result.then(() => {
      this._getClientProfiles();
    });
  }

  updateClient(profile?: ClientProfile){
    const modalRef = this.modalService.open(UpdateClientModalComponent);
    modalRef.componentInstance.FullName = profile.fullName;
    modalRef.componentInstance.UserName = profile.userName;
    modalRef.componentInstance.Email = profile.email;
    modalRef.componentInstance.AccessCode = profile.accessCode;
    modalRef.componentInstance.Id = profile.id;
    modalRef.result.then(() => {
      this._getClientProfiles();
    });
  }
}
