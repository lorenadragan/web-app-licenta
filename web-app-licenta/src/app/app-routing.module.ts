import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AuthGuard } from './auth/auth.guard';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { HomeComponent } from './home/home.component';
import { AppliancesComponent } from './user/appliances/appliances.component';
import { ApplicationMenuComponent } from './user/application-menu/application-menu.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserComponent } from './user/user/user.component';


const routes: Routes = [
  {path: '', redirectTo: '/user/login', pathMatch: 'full'},
  {path: 'user', component: UserComponent,
    children: [
      {path: 'registration', component: RegistrationComponent},
      {path: 'login', component: LoginComponent }
    ]
  },
  {path: "app-menu", component: ApplicationMenuComponent, canActivate:[AuthGuard],
  children: [
    {path: "", redirectTo: "home", pathMatch: "full"},
    {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'appliances', component: AppliancesComponent, canActivate: [AuthGuard],  data: {permittedRoles: ['Customer']}},
    {path: 'adminpanel', component: AdminPanelComponent, canActivate: [AuthGuard], data: {permittedRoles: ['Admin']}}
  ]},
  {path: 'forbidden', component: ForbiddenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
